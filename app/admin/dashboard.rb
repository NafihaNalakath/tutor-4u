ActiveAdmin.register_page "Dashboard" do

 content title: proc{ I18n.t("active_admin.dashboard") } do
    
    columns do
      
      section "new user" do 
        table_for User.order("name").limit(4) do
            column :name
        end
    end


      column do
        panel "Info" do
          para "Welcome to ActiveAdmin."
        end
      end

    end
    section "Search User" do
    div do
      render "search_user"
    end
  end
  
  end 
 end
