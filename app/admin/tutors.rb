ActiveAdmin.register Tutor do
  permit_params :description, :user_id, :hourly_rate, :upgrade, :avatar_file_name, :headline, :user
 	form do |f|
		inputs "Details" do
			input :user
			input :hourly_rate
			input :headline
			input :upgrade
			input :description

		end
		actions
	end



    end


# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
