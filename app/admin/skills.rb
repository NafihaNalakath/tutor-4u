ActiveAdmin.register Skill do
  permit_params :name,:Standerd

  form do |f|
		inputs "Details" do
			input :name
			input :Standerd
		end
		actions
	end
	  index do
    selectable_column
    id_column
    column :name
    column :Standerd
    actions
  end
 
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
